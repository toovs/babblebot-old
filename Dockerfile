FROM toovs/babblebot:0.4.1.0
COPY . /babblebot
WORKDIR /babblebot
CMD ["/tini", "--", "stack", "exec", "--", "babblebot"]
