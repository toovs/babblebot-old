{-# LANGUAGE OverloadedStrings #-}

module Timers
  ( startTimers
  , statusUpdate
  , flushLogs
  , runCommands
  , viewerStats
  ) where

import System.IO
import System.Directory (createDirectoryIfMissing)
import GHC.Int (Int64)
import Data.Char (toLower)
import Data.Scientific (scientific)
import Data.Aeson (Value(..), decode, object)
import Data.Acid (AcidState, query, update)
import Data.Time.Clock (UTCTime, diffUTCTime, getCurrentTime)
import Data.Time.Format (formatTime, defaultTimeLocale)
import Control.Lens ((^.), (.~), (&))
import Control.Concurrent (forkIO)
import Control.Concurrent.Timer (oneShotTimer)
import Control.Concurrent.Suspend (sDelay)
import Network.IRC.Client
import qualified Network.Wreq as W
import qualified Text.Read as TR (readMaybe)
import qualified Data.List as L (filter, find, concat, intercalate, (\\))
import qualified Data.Text as T (unpack, pack)
import qualified Data.Text.Lazy as TL (pack)
import qualified Data.Sequence as S (Seq, filter, index)
import qualified Data.Vector as V (head, any, concat, toList, fromList)
import qualified Data.Yaml as Y (ParseException, decodeFileEither, (.=))
import qualified Data.ByteString.Char8 as C (pack)
import qualified Data.HashMap.Strict as HM (lookup)

import State
import Commands
import Config
import Util

startTimers :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> IO ()
startTimers acid irc b c = do
  logMsg acid c Info "Starting Timers"
  -- run notices
  groups <- query acid $ FetchNoticeGroups $ channel c
  mapM_ (\group -> update acid $ UnreadyGroup (channel c) (groupName group) False) groups
  mapM_ (\group -> oneShotTimer (update acid $ ReadyGroup (channel c) (groupName group)) (sDelay $ interval group)) groups
  -- run timers
  mapM_ (\(timer,delay) -> oneShotTimer (timer acid irc b c delay) (sDelay delay)) [(noticeHandler,30),(hostCheck,60),(viewerStats,60),(updateFortnite,60),(updatePubg,60),(updateSubs,300)]
  mapM_ (\(timer,delay) -> timer acid irc b c delay) [(runCommercials,3600)]

flushLogs :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> IO ()
flushLogs state irc b c = do
  chanLogs <- query state $ FetchLogs $ channel c
  createDirectoryIfMissing True $ "logs/" ++ channel c
  errFile  <- openFile ("logs/" ++ channel c ++ "/error.log") AppendMode
  infoFile <- openFile ("logs/" ++ channel c ++ "/info.log") AppendMode
  mapM_ (pushLog errFile infoFile) chanLogs
  hClose errFile >> hClose infoFile
  update state $ RemoveLogs $ channel c
  where
    pushLog errFile infoFile log' = do
      let (file,lvl) = case level log' of
                         Error -> (errFile," [ERROR]: ")
                         Info  -> (infoFile," [INFO]: ")
          time = formatTime defaultTimeLocale "%F %T" $ timestamp log'
      hPutStrLn file $ "| " ++ time ++ lvl ++ State.message log'

runCommands :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> IO ()
runCommands state irc b c = do
  cmds <- query state $ FetchCommandQ $ channel c
  mapM (\(name,args) -> runIRCAction (runCmd c (bot b) name args False) irc) cmds
  update state $ FlushCommandQ $ channel c

statusUpdate :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> IO ()
statusUpdate state irc b c = do
  prevStatus <- query state $ Status $ channel c
  chan_id <- query state $ ChannelID $ channel c
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Client-ID" .~ [C.pack $ client_id b]
      url  = "https://api.twitch.tv/kraken/streams?channel=" ++ chan_id
  rsp' <- safeGetWith opts url
  case rsp' of
    Left err -> logMsg state c Error err
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ()
        Just (Object o) ->
          case HM.lookup "streams" o of
            Nothing -> pure ()
            Just (Array a) ->
              if length a == 0 then do
                if not prevStatus then pure ()
                else do
                  logMsg state c Info $ "[statusUpdate]: Going Offline"
                  update state (UpdateStatus (channel c) False)
                  -- stop notices(greetingHandler,3600)
                  groups <- query state $ FetchNoticeGroups $ channel c
                  mapM_ (\group -> update state $ UnreadyGroup (channel c) (groupName group) False) groups
                  -- reset db stuff
                  update state (ResetHosts $ channel c)
                  update state (ResetPubg $ channel c)
                  update state (ResetFortnite $ channel c)
              else
                case V.head a of
                  Object o ->
                    case HM.lookup "channel" o of
                      Nothing -> pure ()
                      Just (Object o) -> do
                        let Just game = HM.lookup "game" o
                            Just status = HM.lookup "status" o
                            Just logo = HM.lookup "logo" o
                        update state (UpdateStatus (channel c) True)
                        if prevStatus then pure ()
                        else do
                          logMsg state c Info $ "[statusUpdate]: Going Online"
                          startTimers state irc b c
                          forkIO $ resetSubs state b c
                          -- post to discord
                          discordChan' <- query state $ FetchSetting (channel c) "discord:channel-id" Nothing
                          case (discord_token b, discordChan') of
                            ((Just discordToken),(Just discordChan)) -> do
                              let opts = W.defaults & W.header "Authorization" .~ [C.pack $ "Bot " ++ discordToken] & W.header "User-Agent" .~ [C.pack "DiscordBot (https://gitlab.com/toovs/babblebot, 0.1)"]
                              Just display <- query state $ FetchSetting (channel c) "channel:display-name" (Just $ channel c)
                              Just message <- query state $ FetchSetting (channel c) "discord:live-message" (Just "")
                              -- message <- foldM (\acc (str,f) -> parseVar acc str (f (db state) (botConfig state) chan nick args)) content cmdVars
                              safePostWith opts ("https://discordapp.com/api/channels/" ++ discordChan ++ "/messages") $ object [ "content" Y..= message, "embed" Y..= object [ "author" Y..= object [ "name" Y..= display ], "title" Y..= status, "url" Y..= ("http://twitch.tv/" ++ channel c), "thumbnail" Y..= object [ "url" Y..= logo ], "fields" Y..= V.fromList [ object [ "name" Y..= ("Now Playing"::String), "value" Y..= game ] ] ] ]
                              pure ()
                            _ -> pure ()

hostCheck :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
hostCheck st irc b c delay = do
  status <- query st $ Status $ channel c
  if not status then pure ()
  else do
    hostM <- query st $ FetchSetting (channel c) "channel:host-message" Nothing
    autoM <- query st $ FetchSetting (channel c) "channel:autohost-message" Nothing
    case (hostM,autoM) of
      (Nothing,Nothing) -> pure ()
      _ -> do
        chan_id <- query st $ ChannelID $ channel c
        hosts <- query st $ FetchHosts $ channel c
        let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Client-ID" .~ [C.pack $ client_id b]
        rsp' <- safeGetWith opts ("https://api.twitch.tv/kraken/channels/" ++ chan_id ++ "/hosts")
        case rsp' of
          Left err -> logMsg st c Error err
          Right rsp -> do
            let body = decode $ rsp ^. W.responseBody :: Maybe Value
            case body of
              Nothing -> pure ()
              Just (Object o) ->
                case HM.lookup "hosts" o of
                  Nothing -> pure ()
                  Just (Array a) ->
                    mapM_ (\(Object o) -> let Just (String host') = HM.lookup "host_id" o in let host = T.unpack host' in
                      if elem host hosts then pure ()
                      else do
                        update st $ AddHost (channel c) host
                        shoutout host hostM autoM
                     ) (V.toList a)
    oneShotTimer (hostCheck st irc b c delay) (sDelay delay) >> pure ()
  where
    shoutout host hostM autoM = do
      let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Client-ID" .~ [C.pack $ client_id b]
      rsp' <- safeGetWith opts ("https://api.twitch.tv/kraken/streams/" ++ host)
      case rsp' of
        Left err -> logMsg st c Error err
        Right rsp -> do
          let body = decode $ rsp ^. W.responseBody :: Maybe Value
          case body of
            Nothing -> pure ()
            Just (Object o) -> do
              case HM.lookup "stream" o of
                Nothing -> pure ()
                Just (Object o) -> do
                  case HM.lookup "channel" o of
                    Nothing -> pure ()
                    Just (Object o') -> do
                      let Just (String hostname) = HM.lookup "display_name" o'
                          Just (String url)      = HM.lookup "url" o'
                          Just (String game)     = HM.lookup "game" o'
                          Just (Number viewers)  = HM.lookup "viewers" o
                      case hostM of
                        Nothing -> pure ()
                        Just m -> do
                          parsedM1 <- parseVar m "name" (\_ -> pure $ T.unpack hostname)
                          parsedM2 <- parseVar parsedM1 "url" (\_ -> pure $ T.unpack url)
                          parsedM3 <- parseVar parsedM2 "game" (\_ -> pure $ T.unpack game)
                          parsedM  <- parseVar parsedM3 "viewers" (\_ -> pure $ show $ truncate viewers)
                          runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel c) (Right $ C.pack parsedM)) irc
                Just Null -> do
                  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Client-ID" .~ [C.pack $ client_id b]
                  rsp' <- safeGetWith opts ("https://api.twitch.tv/kraken/channels/" ++ host)
                  case rsp' of
                    Left err -> logMsg st c Error err
                    Right rsp -> do
                      let body = decode $ rsp ^. W.responseBody :: Maybe Value
                      case body of
                        Nothing -> pure ()
                        Just (Object o) -> do
                          let Just (String hostname) = HM.lookup "display_name" o
                              Just (String url)      = HM.lookup "url" o
                              Just (String game)     = HM.lookup "game" o
                          case autoM of
                            Nothing -> pure ()
                            Just m -> do
                              parsedM1 <- parseVar m "name" (\_ -> pure $ T.unpack hostname)
                              parsedM2 <- parseVar parsedM1 "url" (\_ -> pure $ T.unpack url)
                              parsedM  <- parseVar parsedM2 "game" (\_ -> pure $ T.unpack game)
                              runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel c) (Right $ C.pack parsedM)) irc

noticeHandler :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
noticeHandler state irc b c delay = do
  status <- query state $ Status $ channel c
  if not status then pure ()
  else do
    oneShotTimer (noticeHandler state irc b c delay) (sDelay delay)
    groups <- query state $ FetchNoticeGroups $ channel c
    let readyGroups = S.filter readyFilter groups
    if length readyGroups == 0 then pure ()
    else do
      let group = foldl1 maxFold readyGroups
      update state $ UnreadyGroup (channel c) (groupName group) True
      oneShotTimer (update state $ ReadyGroup (channel c) (groupName group)) (sDelay $ interval group)
      notices <- query state $ FetchNotices (channel c) (groupName group)
      runIRCAction (postNotice $ S.index notices 0) irc
  where
    readyFilter g = ready g == True && length (notices g) > 0
    maxFold acc x = if interval acc > interval x then acc else x
    postNotice notice = runCmd c "" (map toLower $ cmdName notice) [] False

updateSubs :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
updateSubs state irc b c delay = do
  updateSubsOffset 0
  status <- query state $ Status $ channel c
  if status then oneShotTimer (updateSubs state irc b c delay) (sDelay delay) >> pure () else pure ()
  where
    updateSubsOffset offset = do
      let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
      cid <- query state $ ChannelID $ channel c
      rsp' <- safeGetWith opts ("https://api.twitch.tv/kraken/channels/" ++ cid ++ "/subscriptions?limit=100&direction=desc&offset=" ++ show offset)
      case rsp' of
        Left err -> pure ()
        Right rsp -> do
          let body = decode $ rsp ^. W.responseBody :: Maybe Value
          case body of
            Nothing -> pure ()
            Just (Object o) -> do
              case HM.lookup "subscriptions" o of
                Nothing -> pure ()
                Just (Array a) -> do
                  subs <- query state $ FetchSubs $ channel c
                  let names = map (\(Object o) -> let Just (Object u) = HM.lookup "user" o in let Just (String n) = HM.lookup "name" u in T.unpack n) (V.toList a)
                      newNames = names L.\\ subs
                  mapM_ (\n -> update state $ AddSub (channel c) n) newNames
                  if length names > length newNames then pure ()
                  else updateSubsOffset $ offset + 100

viewerStats :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
viewerStats st irc b c delay = do
  status <- query st $ Status $ channel c
  enabled <- query st $ FetchSetting (channel c) "viewerstats:enabled" Nothing
  if not status || enabled == Nothing then pure ()
  else do
    let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"]
    rsp' <- safeGetWith opts ("http://tmi.twitch.tv/group/user/" ++ channel c ++ "/chatters")
    case rsp' of
      Left err -> pure ()
      Right rsp -> do
        let body = decode $ rsp ^. W.responseBody :: Maybe Value
        case body of
          Nothing -> pure ()
          Just (Object o) -> do
            case HM.lookup "chatters" o of
              Nothing -> pure ()
              Just (Object o) -> do
                case HM.lookup "viewers" o of
                  Nothing -> pure ()
                  Just (Array v) -> do
                    case HM.lookup "moderators" o of
                      Nothing -> pure ()
                      Just (Array m) -> do
                        case HM.lookup "vips" o of
                          Nothing -> pure ()
                          Just (Array p) -> do
                            let viewers = map (\(String s) -> T.unpack s) $ V.toList $ V.concat [v,m,p]
                            update st $ AddWatchTime (channel c) viewers 1
                            pure ()
    oneShotTimer (viewerStats st irc b c delay) (sDelay delay) >> pure ()

runCommercials :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
runCommercials st irc b c delay = do
  status <- query st $ Status $ channel c
  if not status then pure ()
  else do
    numR'' <- query st $ FetchSetting (channel c) "commercials:per-hour" Nothing
    case numR'' of
      Nothing -> pure ()
      Just numR' -> do
        case (TR.readMaybe numR' :: Maybe Int) of
          Nothing -> pure ()
          Just numR -> do
            now <- getCurrentTime
            coms <- query st $ FetchCommercials $ channel c
            if length coms == 0 then runCommercial coms numR now
            else do
              let lastC = fst $ last coms
                  diffTime = round $ toRational (diffUTCTime now lastC) :: Int64
              if diffTime < 480 then oneShotTimer (runCommercials st irc b c delay) (sDelay (480 - diffTime)) >> pure () else runCommercial coms numR now
  where
    pastHour now com = toRational (diffUTCTime now (fst com)) < 3600
    runCommercial coms numR now = do
      let commercials = L.filter (pastHour now) coms
          num = foldl (\acc com -> acc + (snd com)) 0 commercials
      if num >= numR then oneShotTimer (runCommercials st irc b c delay) (sDelay delay) >> pure ()
      else do
        cid  <- query st $ ChannelID $ channel c
        let opts  = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
            url   = "https://api.twitch.tv/kraken/channels/" ++ cid ++ "/commercial"
            data' = object [("length", Number $ scientific (toInteger (numR - num) * 30) 0)]
        logMsg st c Info $ "[runCommercials]: POST " ++ url ++ " " ++ show data'
        rsp' <- safePostWith opts url data'
        case rsp' of
          Left err -> logMsg st c Error err >> oneShotTimer (runCommercials st irc b c delay) (sDelay delay) >> pure ()
          Right rsp -> do
            let body = decode $ rsp ^. W.responseBody :: Maybe Value
            case body of
              Nothing -> pure ()
              Just (Object o) -> do
                logMsg st c Info $ "[runCommercials]: RSP " ++ (show o)
                update st $ AddCommercial (channel c) now (numR - num)
                notice' <- query st $ FetchSetting (channel c) "commercials:notice" Nothing
                submode <- query st $ FetchSetting (channel c) "commercials:submode" Nothing
                case notice' of
                  Nothing -> pure ()
                  Just notice -> runIRCAction (runCmd c "" (map toLower notice) [] False) irc
                case submode of
                  Nothing -> pure ()
                  Just _ -> do
                    let secs = round $ toRational $ (numR - num) * 30 :: Int64
                    runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel c) (Right $ C.pack "/subscribers")) irc
                    oneShotTimer (runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel c) (Right $ C.pack "/subscribersoff")) irc) (sDelay secs)
                    pure ()
                runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel c) (Right $ C.pack $ show (numR - num) ++ " commercials have been run.")) irc
                oneShotTimer (runCommercials st irc b c delay) (sDelay delay)
                pure ()

updateFortnite :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
updateFortnite st irc b c delay = do
  status <- query st $ Status $ channel c
  if not status then pure ()
  else do
    fortToken' <- query st $ FetchSetting (channel c) "fortnite:token" Nothing
    Just fortPlatform <- query st $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
    case fortToken' of
      Nothing -> pure ()
      Just fortToken -> do
        let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
        fortName' <- query st $ FetchSetting (channel c) "fortnite:name" Nothing
        case fortName' of
          Nothing -> pure ()
          Just fortName -> do
            let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
            rsp' <- safeGetWith opts url
            case rsp' of
              Left err -> logMsg st c Error err
              Right rsp -> do
                let body = decode $ rsp ^. W.responseBody :: Maybe Value
                case body of
                  Nothing -> pure ()
                  Just (Object o) ->
                    case HM.lookup "recentMatches" o of
                      Nothing -> pure ()
                      Just (Array a) -> do
                        prevMatches <- query st $ FetchFortniteMatches (channel c)
                        let matches = map (\(Object o) -> let Just (Number n) = HM.lookup "id" o in round n) (V.toList a)
                            newMatches = matches L.\\ prevMatches
                        update st $ UpdateFortniteMatches (channel c) newMatches
                        let stats = if length prevMatches > 0 then map (parseMatch a) newMatches else []
                        mapM_ (\s -> update st $ UpdateFortniteStat (channel c) s) (L.concat stats)
    oneShotTimer (updateFortnite st irc b c delay) (sDelay delay) >> pure ()
  where
    parseMatch matches mid = do
      let Just (Object match) = L.find (\(Object m) -> let Just (Number n) = HM.lookup "id" m in round n == mid) matches
      map (\key -> let Just s = HM.lookup (T.pack key) match in parseStat key s) ["top1", "kills"]
    parseStat key stat = case stat of
      String s -> StringStat key $ T.unpack s
      Number n -> IntStat key (round n)

updatePubg :: AcidState BabbleState -> IRCState IrcState -> BotConfig -> ChannelConfig -> Int64 -> IO ()
updatePubg st irc b c delay = do
  status <- query st $ Status $ channel c
  if not status then pure ()
  else do
    pubgToken' <- query st $ FetchSetting (channel c) "pubg:token" Nothing
    Just pubgRegion <- query st $ FetchSetting (channel c) "pubg:region" (Just "pc-na")
    case pubgToken' of
      Nothing -> pure ()
      Just pubgToken -> do
        let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "Authorization" .~ [C.pack $ "Bearer " ++ pubgToken]
        pubgID' <- query st $ FetchSetting (channel c) "pubg:id" Nothing
        case pubgID' of
          Just pubgID -> pure ()
          Nothing -> do
            pubgName' <- query st $ FetchSetting (channel c) "pubg:name" Nothing
            case pubgName' of
              Nothing -> pure ()
              Just pubgName -> do
                let url = "https://api.pubg.com/shards/" ++ pubgRegion ++ "/players?filter%5BplayerNames%5D=" ++ pubgName
                logMsg st c Info $ "[updatePubg]: GET " ++ url
                rsp' <- safeGetWith opts url
                case rsp' of
                  Left err -> logMsg st c Error err
                  Right rsp -> do
                    let body = decode $ rsp ^. W.responseBody :: Maybe Value
                    case body of
                      Nothing -> pure ()
                      Just (Object o) ->
                        case HM.lookup "data" o of
                          Nothing -> pure ()
                          Just (Array a) ->
                            if length a == 0 then pure ()
                            else
                              case V.head a of
                                Object o ->
                                  case HM.lookup "id" o of
                                    Nothing -> pure ()
                                    Just (String pid) -> update st $ UpdateSetting (channel c) "pubg:id" (T.unpack pid)
        pubgID' <- query st $ FetchSetting (channel c) "pubg:id" Nothing
        case pubgID' of
          Nothing -> pure ()
          Just pubgID -> do
            let url = "https://api.pubg.com/shards/" ++ pubgRegion ++ "/players/" ++ pubgID
            rsp' <- safeGetWith opts url
            case rsp' of
              Left err -> logMsg st c Error err
              Right rsp -> do
                let body = decode $ rsp ^. W.responseBody :: Maybe Value
                case body of
                  Nothing -> pure ()
                  Just (Object o) ->
                    case HM.lookup "data" o of
                      Nothing -> pure ()
                      Just (Object o) ->
                        case HM.lookup "relationships" o of
                          Nothing -> pure ()
                          Just (Object o) ->
                            case HM.lookup "matches" o of
                              Nothing -> pure ()
                              Just (Object o) ->
                                case HM.lookup "data" o of
                                  Nothing -> pure ()
                                  Just (Array a) -> do
                                    prevMatches <- query st $ FetchPubgMatches (channel c)
                                    let matches = map (\(Object o) -> let Just (String s) = HM.lookup "id" o in T.unpack s) (V.toList a)
                                        newMatches = matches L.\\ prevMatches
                                    update st $ UpdatePubgMatches (channel c) newMatches
                                    stats <- if length prevMatches > 0 then sequence $ map (fetchMatch pubgToken pubgID opts) newMatches else pure []
                                    mapM_ (\s -> update st $ UpdatePubgStat (channel c) s) (L.concat stats)
                                    let winPlace = L.find (\s -> statName s == "winPlace") (L.concat stats)
                                    case winPlace of
                                      Nothing -> pure ()
                                      Just stat -> if intValue stat < 5 then runCommercial else pure ()
    oneShotTimer (updatePubg st irc b c delay) (sDelay delay) >> pure ()
  where
    fetchMatch token pid opts mid = do
      let url = "https://api.pubg.com/shards/pc-na/matches/" ++ mid
      logMsg st c Info $ "[updatePubg]: GET " ++ url
      rsp' <- safeGetWith opts url
      case rsp' of
        Left err -> logMsg st c Error err >> pure []
        Right rsp -> do
          let body = decode $ rsp ^. W.responseBody :: Maybe Value
          case body of
            Nothing -> pure []
            Just (Object o) ->
              case HM.lookup "included" o of
                Nothing -> pure []
                Just (Array a) ->
                  let participants = L.filter (\(Object o) -> let Just s = HM.lookup "type" o in s == "participant") (V.toList a)
                      Just (Object player) = L.find findPlayer participants
                      Just (Object attrs)  = HM.lookup "attributes" player
                      Just (Object stats') = HM.lookup "stats" attrs
                  in pure $ map (\key -> let Just s = HM.lookup (T.pack key) stats' in parseStat key s) ["damageDealt", "headshotKills", "kills", "roadKills", "teamKills", "vehicleDestroys", "winPlace"]
      where
        parseStat key stat' = case stat' of
          String s -> StringStat key $ T.unpack s
          Number n -> IntStat key (round n)
        findPlayer (Object p) =
          let Just (Object attrs)  = HM.lookup "attributes" p
              Just (Object stats') = HM.lookup "stats" attrs
              Just (String pid')   = HM.lookup "playerId" stats'
          in pid' == T.pack pid
    -- this could probably be combined with timer
    runCommercial = do
      numR'' <- query st $ FetchSetting (channel c) "commercials:per-hour" Nothing
      case numR'' of
        Nothing -> pure ()
        Just numR' -> do
          case (TR.readMaybe numR' :: Maybe Int) of
            Nothing -> pure ()
            Just numR -> do
              commercials' <- query st $ FetchCommercials $ channel c
              now <- getCurrentTime
              let lastC = fst $ last commercials'
                  diffTime = toRational (diffUTCTime now lastC)
              if diffTime < 480 then pure ()
              else do
                let commercials = L.filter (pastHour now) commercials'
                    num' = foldl (\acc com -> acc + (snd com)) 0 commercials
                if num' >= numR then pure ()
                else do
                  cid <- query st $ ChannelID $ channel c
                  let num   = if numR - num' > 3 then 3 else numR - num'
                      opts  = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
                      url   = "https://api.twitch.tv/kraken/channels/" ++ cid ++ "/commercial"
                      data' = object [("length", Number $ scientific (toInteger num * 30) 0)]
                  logMsg st c Info $ "[updatePubg]: POST " ++ url ++ " " ++ show data'
                  rsp' <- safePostWith opts url data'
                  case rsp' of
                    Left err -> logMsg st c Error err
                    Right rsp -> do
                      let body = decode $ rsp ^. W.responseBody :: Maybe Value
                      case body of
                        Nothing -> pure ()
                        Just (Object o) -> do
                          logMsg st c Info $ "[runCommercials]: RSP " ++ (show o)
                          update st $ AddCommercial (channel c) now (numR - num)
                          notice' <- query st $ FetchSetting (channel c) "commercials:notice" Nothing
                          case notice' of
                            Nothing -> pure ()
                            Just notice -> runIRCAction (runCmd c "" (map toLower notice) [] False) irc
                          runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel c) (Right $ C.pack $ show num ++ " commercials have been run.")) irc
      where
        pastHour now com = toRational (diffUTCTime now (fst com)) < 3600

resetSubs :: AcidState BabbleState -> BotConfig -> ChannelConfig -> IO ()
resetSubs state b c = do
  update state $ RemoveSubs $ channel c
  cid <- query state $ ChannelID $ channel c
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
      url  = "https://api.twitch.tv/kraken/channels/" ++ cid ++ "/subscriptions?limit=100"
  logMsg state c Info $ "[resetSubs]: GET " ++ url
  rsp' <- safeGetWith opts url
  case rsp' of
    Left err -> pure ()
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ()
        Just (Object o) -> do
          case HM.lookup "_total" o of
            Nothing -> pure ()
            Just (Number n) -> do
              let pages = (ceiling (n / 100.0)) :: Int
              updateSubsPage pages pages
  where
    updateSubsPage pages page = do
      cid <- query state $ ChannelID $ channel c
      let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
          url  = "https://api.twitch.tv/kraken/channels/" ++ cid ++ "/subscriptions?limit=100&offset=" ++ show (100 * (pages - page))
      rsp' <- safeGetWith opts url
      case rsp' of
        Left err -> logMsg state c Error err
        Right rsp -> do
          let body = decode $ rsp ^. W.responseBody :: Maybe Value
          case body of
            Nothing -> pure ()
            Just (Object o) -> do
              case HM.lookup "subscriptions" o of
                Nothing -> pure ()
                Just (Array a) -> do
                  let names = map (\(Object o) -> let Just (Object u) = HM.lookup "user" o in let Just (String n) = HM.lookup "name" u in T.unpack n) (V.toList a)
                  mapM_ (\n -> update state $ AddSub (channel c) n) names
                  if page == 1 then pure () else updateSubsPage pages $ page - 1
