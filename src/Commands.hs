{-# LANGUAGE OverloadedStrings, RecordWildCards #-}

module Commands
  ( runCmd
  , runDiscordCmd
  , cmdVars
  ) where

import GHC.Int (Int64)
import System.Random.MWC (create)
import System.Time.Utils (renderSecs)
import Data.Char (toLower)
import Data.Word (Word32)
import Data.List.Split (splitOn)
import Data.Scientific (scientific)
import Data.Time.Clock (UTCTime(..), diffUTCTime, addUTCTime, getCurrentTime, secondsToDiffTime)
import Data.Time.Format (formatTime, defaultTimeLocale, parseTimeM, iso8601DateFormat)
import Data.Time.Format.Human (humanReadableTime)
import Data.Random.Extras (safeChoices)
import Data.Random (runRVar)
import Data.Aeson (Value(..), decode, encode, object)
import Data.Acid (AcidState, makeAcidic, createArchive, query, update)
import Data.UUID.V4
import Control.Lens ((^.), (.~), (&), (%~))
import Control.Monad (foldM)
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (forkIO)
import Control.Concurrent.Timer (oneShotTimer)
import Control.Concurrent.Suspend (sDelay)
import Control.Concurrent.Async (async, wait)
import Network.IRC.Client
import qualified Text.Read as TR (readMaybe)
import qualified Data.UUID as UUID
import qualified Data.List as L (intercalate, find)
import qualified Data.Text as T (unpack, pack)
import qualified Data.Vector as V (head, toList)
import qualified Data.Foldable as F (toList)
import qualified Data.Sequence as S (index, findIndexL)
import qualified Data.ByteString.Char8 as C (pack)
import qualified Data.ByteString.Lazy.Char8 as LC (unpack)
import qualified Data.HashMap.Strict as HM (lookup)
import qualified Data.Yaml as Y ((.=))
import qualified Wuss as WU (runSecureClient)
import qualified Network.WebSockets as WS (sendClose, sendTextData)
import qualified Network.Wreq as W

import State
import Config
import Util
import Agent

builtinCommands = [("echo", commandEcho, Mod, Mod), ("set", commandSet, Mod, Mod), ("unset", commandUnset, Mod, Mod), ("cleandb", commandClean, Mod, Mod), ("bits", commandBits, Mod, Mod), ("game", commandGame, All, Mod), ("title", commandTitle, All, Mod), ("command", commandCmd, Mod, Mod), ("counters", commandCounters, Mod, Mod), ("phrases", commandPhrases, Mod, Mod), ("notices", commandNotices, Mod, Mod), ("multi", commandMulti, All, Mod), ("greetings", commandGreetings, Mod, Mod), ("giveaway", commandGiveaway, Mod, Mod), ("poll", commandPoll, Mod, Mod), ("moderation", commandModeration, Mod, Mod), ("permit", commandPermit, Mod, Mod), ("watchtime", commandWatchtime, Mod, Mod), ("clip", commandClip, All, All), ("runads", commandCommercials, Mod, Mod), ("genwebauth", commandWebAuth, Mod, Mod), ("listads", commandListCommercials, Mod, Mod), ("listsettings", commandListSettings, Mod, Mod), ("unmod", commandUnmod, Mod, Mod)]

cmdVars = [("args", argsVar), ("cmd", cmdVar), ("user", userVar), ("channel", chanVar), ("uptime",  uptimeVar), ("watchtime", watchtimeVar), ("watchrank", watchrankVar), ("watchranks", watchranksVar), ("followage", followageVar), ("subcount", subcountVar), ("followcount", followVar), ("countdown", countdownVar), ("counterinc", counterincVar), ("counter", counterVar), ("phrase", phraseVar), ("hotkey", hotkeyVar), ("obs:scene-change", obsSceneChangeVar), ("youtube:latest-url", youtubeLatestUrlVar), ("youtube:latest-title", youtubeLatestTitleVar), ("fortnite:wins", fortWinsVar), ("fortnite:kills", fortKillsVar), ("fortnite:lifewins", fortLifeWinsVar), ("fortnite:lifekills", fortLifeKillsVar), ("fortnite:solowins", fortSoloWinsVar), ("fortnite:solokills", fortSoloKillsVar), ("fortnite:duowins", fortDuoWinsVar), ("fortnite:duokills", fortDuoKillsVar), ("fortnite:squadwins", fortSquadWinsVar), ("fortnite:squadkills", fortSquadKillsVar), ("fortnite:season-solowins", fortSeasonSoloWinsVar), ("fortnite:season-solokills", fortSeasonSoloKillsVar), ("fortnite:season-duowins", fortSeasonDuoWinsVar), ("fortnite:season-duokills", fortSeasonDuoKillsVar), ("fortnite:season-squadwins", fortSeasonSquadWinsVar), ("fortnite:season-squadkills", fortSeasonSquadKillsVar), ("pubg:damage", pubgDmgVar), ("pubg:headshots", pubgHeadshotsVar), ("pubg:kills", pubgKillsVar), ("pubg:roadkills", pubgRoadKillsVar), ("pubg:teamkills", pubgTeamKillsVar), ("pubg:vehiclesDestroyed", pubgVehiclesDestroyedVar), ("pubg:wins", pubgWinsVar)]

buildCmds pre cmds = map (\(cmd,f,p,pa) -> (Command { cmdName = pre ++ cmd, permLevel = p, argPermLevel = pa, aliases = [], lastRun = Nothing }, buildCmd f)) builtinCommands ++ map (\c -> (c, buildCmd $ customCmd $ cmdContent c)) (F.toList cmds)

buildDiscordCmds pre cmds = map (\c -> (c, cmdContent c)) (F.toList cmds)

runCmd :: ChannelConfig -> String -> String -> [String] -> Bool -> IRC IrcState ()
runCmd chan nick cname args auth = do
  state <- fetchIRCState
  commands <- liftIO $ query (db state) (FetchCommands $ channel chan)
  prefix' <- liftIO $ query (db state) $ FetchSetting (channel chan) "command:prefix" (Just "!")
  case prefix' of
    Nothing -> pure ()
    Just prefix -> do
      mapM_ (\(cmd,f) -> do
        if cmdName cmd == map toLower cname || any (\a -> a == map toLower cname) (aliases cmd) then do
          now <- liftIO getCurrentTime
          case lastRun cmd of
            Nothing -> do
              liftIO $ update (db state) $ RunCom (channel chan) (cmdName cmd) now
              authorized <- liftIO $ if auth then permCheck (db state) (botConfig state) cmd else pure True
              if authorized then f chan args nick else pure ()
            Just lastR ->
              let diffTime = round $ toRational (diffUTCTime now lastR) :: Int64
              in if diffTime <= 5 then pure () else do
                liftIO $ update (db state) $ RunCom (channel chan) (cmdName cmd) now
                authorized <- liftIO $ if auth then permCheck (db state) (botConfig state) cmd else pure True
                if authorized then f chan args nick else pure ()
        else pure ()
       ) (buildCmds prefix commands)
  where
   permCheck st b c =
     case if length args == 0 then permLevel c else argPermLevel c of
       All -> pure True
       Sub -> do
         subs <- query st $ FetchSubs $ channel chan
         pure $ if elem nick subs then True else False
       Mod -> do
         mods <- query st $ FetchMods $ channel chan
         if elem nick mods then pure True else modCheck st b chan nick

runDiscordCmd :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> String -> IO ()
runDiscordCmd db botC chan cid cname = do
 commands <- liftIO $ query db (FetchCommands $ channel chan)
 Just prefix <- liftIO $ query db $ FetchSetting (channel chan) "command:prefix" (Just "!")
 mapM_ (\(cmd,f) -> do
   if cmdName cmd == map toLower cname || any (\a -> a == map toLower cname) (aliases cmd) then do
     let Just token = discord_token botC
         opts = W.defaults & W.header "Authorization" .~ [C.pack $ "Bot " ++ token] & W.header "User-Agent" .~ [C.pack "DiscordBot (https://gitlab.com/toovs/babblebot, 0.1)"]
     safePostWith opts ("https://discordapp.com/api/channels/" ++ cid ++ "/messages") $ object [ "content" Y..= cmdContent cmd ]
     pure ()
   else pure ()
  ) (buildDiscordCmds prefix commands)

buildCmd :: (ChannelConfig -> [String] -> String -> IRC IrcState String) -> ChannelConfig -> [String] -> String -> IRC IrcState ()
buildCmd f chan args nick = do
  state <- fetchIRCState
  content <- f chan args nick
  parsedContent <- liftIO $ foldM (\acc (str,f) -> parseVar acc str (f (db state) (botConfig state) chan nick args)) content cmdVars
  me <- liftIO $ query (db state) $ FetchSetting (channel chan) "channel:me" Nothing
  case me of
    Nothing -> send $ Privmsg (T.pack $ "#" ++ channel chan) (Right $ T.pack parsedContent)
    Just _  -> send $ Privmsg (T.pack $ "#" ++ channel chan) (Right $ T.pack ("/me " ++ parsedContent))

customCmd :: String -> ChannelConfig -> [String] -> String -> IRC IrcState String
customCmd content chan args nick =
  let pre = if length args > 0 && head (last args) == '@' then last args ++ " -> " else ""
  in pure $ pre ++ content

argsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
argsVar s b c nick cargs args = do
  if length args == 0 then pure (L.intercalate " " cargs) else do
    case (TR.readMaybe (args!!0) :: Maybe Int) of
      Nothing -> pure ""
      Just num -> pure $ if length cargs < num then "" else cargs!!(num-1)

cmdVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
cmdVar s b c nick cargs args = do
  if length args == 0 then pure "" else do
    update s $ PushCommandQ (channel c) (args!!0) (drop 1 args)
    pure ""

hotkeyVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
hotkeyVar s b c nick cargs args = do
  if length args == 0 then pure "" else do
    let action = Input $ map (\a -> readArg a) args
    update s $ AddAction (channel c) action
    pure ""
  where
    readArg arg = case (TR.readMaybe arg :: Maybe Word32) of
                    Nothing -> 0 :: Word32
                    Just num -> num

obsSceneChangeVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
obsSceneChangeVar s b c nick cargs args = do
  if length args == 0 then pure "" else do
    let action = SceneChange (L.intercalate " " args)
    update s $ AddAction (channel c) action
    pure ""

countdownVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
countdownVar s b c nick cargs args = do
  if length args == 0 then pure "" else do
    now <- getCurrentTime
    let time' = parseTimeM True defaultTimeLocale (iso8601DateFormat (Just "%H:%MZ%Z")) (args!!0) :: Maybe UTCTime
    case time' of
      Nothing -> pure ""
      Just time -> do
        let diffTime = floor $ toRational (diffUTCTime time now) :: Integer
        if diffTime > 0 then pure $ hrTime time now
        else do
          human <- humanReadableTime $ addUTCTime (diffUTCTime time now) now
          pure human

counterVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
counterVar s b c nick cargs args = do
  if length args == 0 then pure "0" else do
    counters <- query s $ FetchCounters (channel c)
    case L.find (\(k,_) -> k == head args) counters of
      Nothing    -> pure "0"
      Just (_,v) -> pure $ show v

phraseVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
phraseVar s b c nick cargs args = do
  if length args == 0 then pure "" else do
    phrases <- query s $ FetchPhrases (channel c)
    case L.find (\(k,_) -> k == head args) phrases of
      Nothing    -> pure ""
      Just (_,v) -> pure v

counterincVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
counterincVar s b c nick cargs args = if length args == 0 then pure "" else update s (IncCounter (channel c) (args!!0)) >> pure ""

chanVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
chanVar s b c nick cargs args = do
  Just display <- query s $ FetchSetting (channel c) "channel:display-name" (Just $ channel c)
  pure display

userVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
userVar s b c nick cargs args = pure nick

watchtimeVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
watchtimeVar s b c nick cargs args = do
  mins <- query s $ FetchWatchTime (channel c) nick
  now <- getCurrentTime
  pure $ hrTime (addUTCTime (fromRational $ toRational $ secondsToDiffTime $ toInteger mins*60) now) now

watchrankVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
watchrankVar s b c nick cargs args = do
  rank <- query s $ FetchWatchRank (channel c) nick
  pure $ show rank

watchranksVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
watchranksVar s b c nick cargs args = do
  if length args == 0 then pure ""
  else do
    case (TR.readMaybe (args!!0) :: Maybe Int) of
      Nothing -> pure ""
      Just num -> do
        ranks <- query s $ FetchWatchRanks (channel c) num
        pure $ L.intercalate ", " ranks

uptimeVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
uptimeVar s b c nick cargs args = do
  let opts = W.defaults & W.header "Client-ID" .~ [C.pack $ client_id b]
  rsp' <- safeGetWith opts $ "https://api.twitch.tv/helix/streams?user_login=" ++ channel c
  case rsp' of
    Left err -> liftIO (logMsg s c Error err) >> pure ""
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ""
        Just (Object o) ->
          case HM.lookup "data" o of
            Nothing -> pure ""
            Just (Array a) ->
              if length a == 0 then pure ""
              else
                case V.head a of
                  Object o ->
                    case HM.lookup "started_at" o of
                      Nothing -> pure ""
                      Just (String started_at) -> do
                        let (Just time1) = parseTimeM False defaultTimeLocale "%FT%TZ" (T.unpack started_at) :: Maybe UTCTime
                        time2 <- getCurrentTime
                        pure $ renderSecs $ truncate $ toRational $ diffUTCTime time2 time1
                  _ -> pure ""

subcountVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
subcountVar s b c nick cargs args = do
  cid <- liftIO $ query s $ ChannelID $ channel c
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
  rsp' <- safeGetWith opts $ "https://api.twitch.tv/kraken/channels/" ++ cid ++ "/subscriptions"
  case rsp' of
    Left err -> liftIO (logMsg s c Error err) >> pure ""
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ""
        Just (Object o) -> do
          case HM.lookup "_total" o of
            Nothing -> pure ""
            Just (Number n) -> pure $ show (round n)

followVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
followVar s b c nick cargs args = do
  cid <- liftIO $ query s $ ChannelID $ channel c
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
  rsp' <- safeGetWith opts $ "https://api.twitch.tv/kraken/channels/" ++ cid
  case rsp' of
    Left err -> liftIO (logMsg s c Error err) >> pure ""
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ""
        Just (Object o) -> do
          case HM.lookup "followers" o of
            Nothing -> pure ""
            Just (Number n) -> pure $ show (round n)

followageVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
followageVar s b c nick cargs args = do
  cid <- liftIO $ query s $ ChannelID $ channel c
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth c] & W.header "Client-ID" .~ [C.pack $ client_id b]
  rsp' <- safeGetWith opts $ "https://api.twitch.tv/kraken/users?login=" ++ nick
  case rsp' of
    Left err -> liftIO (logMsg s c Error err) >> pure ""
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ""
        Just (Object o) -> do
          case HM.lookup "users" o of
            Nothing -> pure ""
            Just (Array a) ->
              if length a == 0 then pure $ "Couldn't find a user matching: " ++ nick
              else case V.head a of
                Object o ->
                  case HM.lookup "_id" o of
                    Nothing -> pure ""
                    Just (String s) -> do
                      rsp' <- safeGetWith opts $ "https://api.twitch.tv/kraken/users/" ++ T.unpack s ++ "/follows/channels/" ++ cid
                      case rsp' of
                        Left err -> pure ""
                        Right rsp -> do
                          let body = decode $ rsp ^. W.responseBody :: Maybe Value
                          case body of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "created_at" o of
                                Nothing -> pure ""
                                Just (String s) -> do
                                 now <- getCurrentTime
                                 let time' = parseTimeM True defaultTimeLocale "%FT%XZ" (T.unpack s) :: Maybe UTCTime
                                 case time' of
                                   Nothing -> pure ""
                                   Just time -> do
                                     let diffTime = floor $ toRational (diffUTCTime now time) :: Integer
                                     if diffTime > 0 then pure $ hrTime now time
                                     else pure "err"

youtubeLatestUrlVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
youtubeLatestUrlVar s b c nick cargs args = do
 if length args == 0 then pure "" else do
   let opts = W.defaults & W.header "Client-ID" .~ [C.pack $ client_id b]
   rsp' <- safeGetWith opts $ "https://decapi.me/youtube/latest_video?id=" ++ head args
   case rsp' of
     Left err -> liftIO (logMsg s c Error err) >> pure ""
     Right rsp ->
       let body = rsp ^. W.responseBody
           splitBody = splitOn " - " (LC.unpack body)
           url = last splitBody
       in pure url

youtubeLatestTitleVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
youtubeLatestTitleVar s b c nick cargs args = do
  if length args == 0 then pure "" else do
    let opts = W.defaults & W.header "Client-ID" .~ [C.pack $ client_id b]
    rsp' <- safeGetWith opts $ "https://decapi.me/youtube/latest_video?id=" ++ head args
    case rsp' of
      Left err -> liftIO (logMsg s c Error err) >> pure ""
      Right rsp ->
        let body = rsp ^. W.responseBody
            splitBody = splitOn " - " (LC.unpack body)
            title = L.intercalate " - " $ take (length splitBody - 1) splitBody
        in pure title

fortLifeWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortLifeWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "folet x = rtnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "lifeTimeStats" o of
                    Nothing -> pure ""
                    Just (Array a) -> do
                      case L.find (findKey "Wins") (V.toList a) of
                        Nothing -> pure ""
                        Just (Object o) ->
                          case HM.lookup "value" o of
                            Nothing -> pure ""
                            Just (String s) -> pure $ T.unpack s
  where
    findKey key (Object o) = case HM.lookup "key" o of
                           Nothing -> False
                           Just (String s) -> key == T.unpack s

fortLifeKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortLifeKillsVar s b c nick cargs args = do
 fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
 Just fortPlatform <- query s $ FetchSetting (channel c) "folet x = rtnite:platform" (Just "pc")
 case fortToken' of
   Nothing -> pure ""
   Just fortToken -> do
     let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
     fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
     case fortName' of
       Nothing -> pure ""
       Just fortName -> do
         let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
         rsp' <- safeGetWith opts url
         case rsp' of
           Left err -> logMsg s c Error err >> pure ""
           Right rsp -> do
             let body = decode $ rsp ^. W.responseBody :: Maybe Value
             case body of
               Nothing -> pure ""
               Just (Object o) ->
                 case HM.lookup "lifeTimeStats" o of
                   Nothing -> pure ""
                   Just (Array a) -> do
                     case L.find (findKey "Kills") (V.toList a) of
                       Nothing -> pure ""
                       Just (Object o) ->
                         case HM.lookup "value" o of
                           Nothing -> pure ""
                           Just (String s) -> pure $ T.unpack s
 where
   findKey key (Object o) = case HM.lookup "key" o of
                          Nothing -> False
                          Just (String s) -> key == T.unpack s


fortSoloWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSoloWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "p2" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "top1" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSoloKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSoloKillsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "p2" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "kills" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortDuoWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortDuoWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "p10" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "top1" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortDuoKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortDuoKillsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "p10" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "kills" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSquadWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSquadWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "p9" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "top1" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSquadKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSquadKillsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "p9" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "kills" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSeasonSoloWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSeasonSoloWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "curr_p2" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "top1" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSeasonSoloKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSeasonSoloKillsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "curr_p2" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "kills" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSeasonDuoWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSeasonDuoWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "curr_p10" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "top1" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSeasonDuoKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSeasonDuoKillsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "curr_p10" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "kills" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSeasonSquadWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSeasonSquadWinsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "curr_p9" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "top1" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortSeasonSquadKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortSeasonSquadKillsVar s b c nick cargs args = do
  fortToken' <- query s $ FetchSetting (channel c) "fortnite:token" Nothing
  Just fortPlatform <- query s $ FetchSetting (channel c) "fortnite:platform" (Just "pc")
  case fortToken' of
    Nothing -> pure ""
    Just fortToken -> do
      let opts = W.defaults & W.header "accept" .~ [C.pack $ "application/vnd.api+json"] & W.header "TRN-Api-Key" .~ [C.pack fortToken]
      fortName' <- query s $ FetchSetting (channel c) "fortnite:name" Nothing
      case fortName' of
        Nothing -> pure ""
        Just fortName -> do
          let url = "https://api.fortnitetracker.com/v1/profile/" ++ fortPlatform ++ "/" ++ fortName
          rsp' <- safeGetWith opts url
          case rsp' of
            Left err -> logMsg s c Error err >> pure ""
            Right rsp -> do
              let body = decode $ rsp ^. W.responseBody :: Maybe Value
              case body of
                Nothing -> pure ""
                Just (Object o) ->
                  case HM.lookup "stats" o of
                    Nothing -> pure ""
                    Just (Object o) -> do
                      case HM.lookup "curr_p9" o of
                        Nothing -> pure ""
                        Just (Object o) -> do
                          case HM.lookup "kills" o of
                            Nothing -> pure ""
                            Just (Object o) -> do
                              case HM.lookup "value" o of
                                Nothing -> pure ""
                                Just (String s) -> pure $ T.unpack s

fortWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortWinsVar s b c nick cargs args = query s (FetchFortniteStat (channel c) "top1") >>= pure . show

fortKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
fortKillsVar s b c nick cargs args = query s (FetchFortniteStat (channel c) "kills") >>= pure . show

pubgDmgVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgDmgVar s b c nick cargs args = query s (FetchPubgStat (channel c) "damageDealt") >>= pure . show

pubgHeadshotsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgHeadshotsVar s b c nick cargs args = query s (FetchPubgStat (channel c) "headshotKills") >>= pure . show

pubgKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgKillsVar s b c nick cargs args = query s (FetchPubgStat (channel c) "kills") >>= pure . show

pubgRoadKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgRoadKillsVar s b c nick cargs args = query s (FetchPubgStat (channel c) "roadKills") >>= pure . show

pubgTeamKillsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgTeamKillsVar s b c nick cargs args = query s (FetchPubgStat (channel c) "teamKills") >>= pure . show

pubgVehiclesDestroyedVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgVehiclesDestroyedVar s b c nick cargs args = query s (FetchPubgStat (channel c) "vehiclesDestroyed") >>= pure . show

pubgWinsVar :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> [String] -> [String] -> IO String
pubgWinsVar s b c nick cargs args = query s (FetchPubgStat (channel c) "winPlace") >>= pure . show

commandEcho :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandEcho chan args nick = do
  if length args < 1 then pure ""
  else do
    pure $ L.intercalate " " args

commandSet :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandSet chan args nick = do
  if length args < 2 then pure ""
  else do
    state <- fetchIRCState
    liftIO $ update (db state) $ UpdateSetting (channel chan) (head args) (L.intercalate " " $ tail args)
    case head args of
      "pubg:token" -> postR
      "fortnite:token" -> postR
      _ -> postM
  where
    postM = pure $ (args!!0) ++ " has been set to: " ++ (L.intercalate " " $ tail args)
    postR = pure $ (args!!0) ++ " has been set."

commandUnset :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandUnset chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    liftIO $ update (db state) $ RemoveSetting (channel chan) (head args)
    pure $ (args!!0) ++ " has been unset."

commandClean :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandClean chan args nick = do
  state <- fetchIRCState
  liftIO $ createArchive $ db state
  pure "the database has been cleaned"

commandBits :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandBits chan args nick = do
  if length args < 1 then pure ""
  else do -- !bits hotkey grenades 4500 23 54
    state <- fetchIRCState
    case head args of
      "hotkey" ->
        if length args < 4 then pure ""
        else do
          case (TR.readMaybe (args!!2) :: Maybe Int) of
            Nothing -> pure "0"
            Just bnum -> do
              let nums = map (\(Just n) -> n) $ filter (/= Nothing) $ map (\s -> TR.readMaybe s :: Maybe Word32) (drop 3 args)
              if length nums == 0 then pure "1"
              else do
                liftIO $ update (db state) $ AddBitEvent (channel chan) (args!!1) bnum nums
                pure $ (args!!1) ++ " bit event has been added."
      "remove" -> do
        if length args < 2 then pure ""
        else do
          liftIO $ update (db state) $ RemoveBitEvent (channel chan) (args!!1)
          pure $ (args!!1) ++ " bit event has been removed."
      "list" -> do
        events <- liftIO $ query (db state) $ FetchBitEvents (channel chan)
        pure $ L.intercalate ", " $ map (\ev -> ename ev) (F.toList events)
      _ -> pure ""

commandCounters :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandCounters chan args nick = do
  if length args < 2 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "set" ->
        if length args < 3 then pure ""
        else do
          case (TR.readMaybe (args!!2) :: Maybe Int) of
            Nothing -> pure ""
            Just num -> do
              liftIO $ update (db state) $ UpdateCounter (channel chan) (args!!1) num
              pure $ (args!!1) ++ " counter has been set to: " ++ (args!!2)
      "inc" -> do
        -- could be rate limited
        liftIO $ update (db state) $ IncCounter (channel chan) (args!!1)
        pure $ (args!!1) ++ " counter has been increased"
      _ -> pure ""

commandPhrases :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandPhrases chan args nick = do
  if length args < 2 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "set" ->
        if length args < 3 then pure ""
        else do
          liftIO $ update (db state) $ UpdatePhrase (channel chan) (args!!1) (L.intercalate " " $ drop 2 args)
          pure $ (args!!1) ++ " phrase has been set to: " ++ (L.intercalate " " $ drop 2 args)
      _ -> pure ""

commandCmd :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandCmd chan args nick = do
  if length args < 2 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "add" ->
        if length args < 3 then pure ""
        else do
          cmds <- liftIO $ query (db state) $ FetchCommands (channel chan)
          case L.find (\cmd -> any (\a -> a == map toLower (args!!1)) (aliases cmd)) cmds of
            Just _  -> pure $ args!!1 ++ " exists as an alias."
            Nothing -> do
              let content = L.intercalate " " $ tail $ drop 1 args
              liftIO $ update (db state) (AddCom (channel chan) (args!!1) content All)
              pure $ (args!!1) ++ " has been added."
      "subadd" ->
        if length args < 3 then pure ""
        else do
          let content = L.intercalate " " $ tail $ drop 1 args
          liftIO $ update (db state) (AddCom (channel chan) (args!!1) content Sub)
          pure $ (args!!1) ++ " has been added."
      "modadd" ->
        if length args < 3 then pure ""
        else do
          let content = L.intercalate " " $ tail $ drop 1 args
          liftIO $ update (db state) (AddCom (channel chan) (args!!1) content Mod)
          pure $ (args!!1) ++ " has been added."
      "remove" -> do
        liftIO $ update (db state) $ DelCom (channel chan) (args!!1)
        pure $ args!!1 ++ " has been removed."
      "alias" ->
        if length args < 3 then pure ""
        else do
          cmds <- liftIO $ query (db state) $ FetchCommands (channel chan)
          case L.find (\c -> cmdName c == args!!1) cmds of
            Nothing  -> pure $ args!!1 ++ " is not an existing command."
            Just cmd -> do
              case L.find (\c -> cmdName c == args!!2 || any (\a -> a == map toLower (args!!2)) (aliases c)) cmds of
                Just _  -> pure $ args!!2 ++ " is an existing command or alias."
                Nothing -> do
                  liftIO $ update (db state) $ AddAlias (channel chan) (args!!1) (args!!2)
                  pure $ args!!2 ++ " has been added as an alias to " ++ (args!!1) ++ "."

      "delalias" -> do
        liftIO $ update (db state) $ DelAlias (channel chan) (args!!1)
        pure $ args!!1 ++ " has been removed as an alias."
      "listalias" -> do
        cmds <- liftIO $ query (db state) $ FetchCommands (channel chan)
        case L.find (\c -> cmdName c == args!!1) cmds of
          Nothing  -> pure $ args!!1 ++ " is not an existing command."
          Just cmd -> do
            aliases <- liftIO $ query (db state) $ FetchAlias (channel chan) (args!!1)
            pure $ "/w " ++ nick ++ " " ++ L.intercalate ", " aliases
      _ -> pure ""

commandGame :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandGame chan args nick = do
  state <- fetchIRCState
  let optsH = W.defaults & W.header "Client-ID" .~ [C.pack $ client_id $ botConfig state]
  let optsK = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth chan] & W.header "Client-ID" .~ [C.pack $ client_id $ botConfig state]
  cid <- liftIO $ query (db state) $ ChannelID $ channel chan
  if length args < 1 then do
    rsp' <- liftIO $ safeGetWith optsK ("https://api.twitch.tv/kraken/channels/" ++ cid)
    case rsp' of
      Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
      Right rsp -> do
        let body = decode $ rsp ^. W.responseBody :: Maybe Value
        case body of
          Nothing -> pure ""
          Just (Object o) ->
            case HM.lookup "game" o of
              Nothing -> pure ""
              Just (String g) -> pure $ T.unpack g
  else do
    rsp' <- liftIO $ safeGetWith optsH ("https://api.twitch.tv/helix/games?name=" ++ L.intercalate " " args)
    case rsp' of
      Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
      Right rsp -> do
        let body = decode $ rsp ^. W.responseBody :: Maybe Value
        case body of
          Nothing -> pure ""
          Just (Object o) ->
            case HM.lookup "data" o of
              Nothing -> pure ""
              Just (Array a) ->
                if length a == 0 then pure $ "Couldn't find a game matching: " ++ L.intercalate " " args
                else case V.head a of
                  Object o ->
                    case HM.lookup "name" o of
                      Nothing -> pure ""
                      Just (String s) -> do
                        rsp' <- liftIO $ safePutWith optsK ("https://api.twitch.tv/kraken/channels/" ++ cid) $ object [ "channel" Y..= object [ "game" Y..= L.intercalate " " args ] ]
                        case rsp' of
                          Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
                          Right rsp -> do
                            let body = decode $ rsp ^. W.responseBody :: Maybe Value
                            case body of
                              Nothing -> pure ""
                              Just (Object o) ->
                                case HM.lookup "game" o of
                                  Nothing -> pure ""
                                  Just (String g) -> pure $ "Game is now set to: " ++ T.unpack g

commandTitle :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandTitle chan args nick = do
  state <- fetchIRCState
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth chan] & W.header "Client-ID" .~ [C.pack $ client_id $ botConfig state]
  cid <- liftIO $ query (db state) $ ChannelID $ channel chan
  if length args < 1 then do
    rsp' <- liftIO $ safeGetWith opts ("https://api.twitch.tv/kraken/channels/" ++ cid)
    case rsp' of
      Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
      Right rsp -> do
        let body = decode $ rsp ^. W.responseBody :: Maybe Value
        case body of
          Nothing -> pure ""
          Just (Object o) ->
            case HM.lookup "status" o of
              Nothing -> pure ""
              Just (String g) -> pure $ T.unpack g
  else do
    rsp' <- liftIO $ safePutWith opts ("https://api.twitch.tv/kraken/channels/" ++ cid) $ object [ "channel" Y..= object [ "status" Y..= L.intercalate " " args ] ]
    case rsp' of
      Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
      Right rsp -> do
        let body = decode $ rsp ^. W.responseBody :: Maybe Value
        case body of
          Nothing -> pure ""
          Just (Object o) ->
            case HM.lookup "status" o of
              Nothing -> pure ""
              Just (String g) -> pure $ "Title is now set to: " ++ T.unpack g

commandNotices :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandNotices chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "addgroup" ->
        if length args < 3 then pure ""
        else do
          case (TR.readMaybe (args!!2) :: Maybe Int64) of
            Nothing -> pure ""
            Just num -> do
              groups <- liftIO $ query (db state) $ FetchNoticeGroups $ channel chan
              group' <- liftIO $ query (db state) $ FetchNoticeGroup (channel chan) (args!!1)
              case group' of
                Just g -> pure $ "Notice group " ++ (args!!1) ++ " already exists."
                Nothing -> do
                  liftIO $ update (db state) $ AddNoticeGroup (channel chan) (args!!1) num
                  group' <- liftIO $ query (db state) $ FetchNoticeGroup (channel chan) (args!!1)
                  case group' of
                    Nothing -> pure ""
                    Just group -> do
                      liftIO $ oneShotTimer (update (db state) $ ReadyGroup (channel chan) (groupName group)) (sDelay $ interval group)
                      pure $ "Notice group " ++ (args!!1) ++ " has been created with interval: " ++ (args!!2)
      "editgroup" ->
        if length args < 4 then pure ""
        else do
          case (TR.readMaybe (args!!3) :: Maybe Int64) of
            Nothing -> pure ""
            Just num -> do
              liftIO $ update (db state) $ EditNoticeGroup (channel chan) (args!!1) (args!!2) num
              pure $ "Notice group " ++ (args!!1) ++ " has been changed to " ++ (args!!2) ++ " with interval: " ++ (args!!3)
      "delgroup" ->
        if length args < 2 then pure ""
        else do
          liftIO $ update (db state) $ DelNoticeGroup (channel chan) (args!!1)
          pure $ "Notice group " ++ (args!!1) ++ " has been removed."
      "remove" ->
        if length args < 3 then pure ""
        else do
          liftIO $ update (db state) $ DelNotice (channel chan) (args!!1) (args!!2)
          pure $ (args!!2) ++ " has been removed from notice group: " ++ (args!!1)
      "add" ->
        if length args < 3 then pure ""
        else do
          cmds <- liftIO $ query (db state) $ FetchCommands (channel chan)
          case S.findIndexL cmdFilter cmds of
            Nothing -> pure $ (args!!2) ++ " is not a known command."
            Just i  -> do
              liftIO $ update (db state) $ AddNotice (channel chan) (args!!1) (S.index cmds i)
              pure $ (args!!2) ++ " has been added to notice group: " ++ (args!!1)
      "list" -> do
        groups <- liftIO $ query (db state) $ FetchNoticeGroups (channel chan)
        pure $ "/w " ++ nick ++ " " ++ (L.intercalate ". " $ map (\g -> groupName g ++ ": " ++ (L.intercalate ", " $ map (\n -> cmdName n) $ F.toList $ notices g)) (F.toList groups))
      _ -> pure ""
  where
    cmdFilter c = cmdName c == (args!!2)

commandMulti :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandMulti chan args nick = do
  state <- fetchIRCState
  if length args == 0 then do
    streams <- liftIO $ query (db state) $ FetchMulti $ channel chan
    if length streams == 0 then pure ""
    else pure $ "http://multistre.am/" ++ channel chan ++ "/" ++ L.intercalate "/" streams
  else do
    liftIO $ update (db state) $ UpdateMulti (channel chan) (head args) (tail args)
    case head args of
      "clear" -> pure $ "!multi has been cleared."
      "set"   -> pure $ "!multi has been set."
      _       -> pure ""

commandGreetings :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandGreetings chan args nick = do
  if length args < 3 then pure ""
  else do
    state <- fetchIRCState
    case (TR.readMaybe (args!!1) :: Maybe Int64) of
      Nothing -> pure ""
      Just num -> do
        liftIO $ update (db state) $ SetGreeting (channel chan) (args!!0) num (L.intercalate " " (drop 2 args))
        pure $ "Channel greeting for " ++ (args!!0) ++ " has been set."

commandPoll :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandPoll chan args nick = pure ""

commandGiveaway :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandGiveaway chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "start" ->
        if length args < 4 then pure ""
        else do
          case (TR.readMaybe (args!!3) :: Maybe Int) of
            Nothing -> pure ""
            Just num -> do
              liftIO $ update (db state) $ StartGiveaway (channel chan) (args!!1) (args!!2) num
              pure $ "Giveaway " ++ (args!!1) ++ " has been started with keyword: " ++ (args!!2)
      "end" ->
        if length args < 2 then pure ""
        else do
          g <- liftIO $ query (db state) $ FetchGiveaway (channel chan) (args!!1)
          case g of
            Nothing -> pure ""
            Just giveaway ->
              let num_winners = if (winners giveaway) > (length $ users giveaway) then (length $ users giveaway) else winners giveaway
                  choices = safeChoices num_winners (users giveaway)
              in case choices of
                   Nothing -> do
                     liftIO $ update (db state) $ EndGiveaway (channel chan) (args!!1)
                     pure $ "Giveaway " ++ (args!!1) ++ " has been ended. There were no winners :("
                   Just vars -> do
                     mwc <- liftIO create
                     winners <- liftIO $ runRVar vars mwc
                     liftIO $ update (db state) $ EndGiveaway (channel chan) (args!!1)
                     case length winners of
                       0 -> pure $ "Giveaway " ++ (args!!1) ++ " has been ended. There were no winners :("
                       1 -> pure $ "Giveaway " ++ (args!!1) ++ " has been ended. The winner is: " ++ head winners ++ "!"
                       _ -> pure $ "Giveaway " ++ (args!!1) ++ " has been ended. The winners are: " ++ (L.intercalate ", " winners) ++ "!"

      _ -> pure ""

commandWatchtime :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandWatchtime chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "blacklist" ->
        if length args < 2 then pure ""
        else do
          liftIO $ update (db state) $ BlacklistWatchTime (channel chan) (args!!1)
          pure $ (args!!1) ++ " has been blacklisted from watchtime."
      _ -> pure ""

commandListCommercials :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandListCommercials chan args nick = do
  state <- fetchIRCState
  commercials <- liftIO $ query (db state) $ FetchCommercials $ channel chan
  now <- liftIO getCurrentTime
  pure $ "/w " ++ nick ++ " " ++ (L.intercalate ", " $ map (\(t,n) -> show (formatTime defaultTimeLocale "%R" t,n)) $ reverse commercials)

commandListSettings :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandListSettings chan args nick = do
  state <- fetchIRCState
  settings <- liftIO $ query (db state) $ FetchSettings $ channel chan
  pure $ "/w " ++ nick ++ " " ++ L.intercalate ", " (map (\(k,v) -> k ++ ": " ++ v) (F.toList settings))

commandClip :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandClip chan args nick = do
  state <- fetchIRCState
  cid <- liftIO $ query (db state) $ ChannelID $ channel chan
  let opts = W.defaults & W.header "Authorization" .~ [C.pack $ "Bearer " ++ oauth chan]
  rsp' <- liftIO $ safePostWith opts "https://api.twitch.tv/helix/clips" $ object [ "broadcaster_id" Y..=  cid ]
  case rsp' of
    Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure ""
        Just (Object o) ->
          case HM.lookup "data" o of
            Nothing -> pure ""
            Just (Array a) ->
              if length a == 0 then pure ""
              else
                case V.head a of
                  Object o ->
                    case HM.lookup "edit_url" o of
                      Nothing -> pure ""
                      Just (String url) -> pure $ T.unpack url

commandCommercials :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandCommercials chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    case (TR.readMaybe (head args) :: Maybe Integer) of
      Nothing -> pure ""
      Just num -> do
        commercials <- liftIO $ query (db state) $ FetchCommercials $ channel chan
        now <- liftIO $ getCurrentTime
        if length commercials == 0 then postR now num
        else
          let lastC = fst $ last commercials
          in if toRational (diffUTCTime now lastC) < 480 then pure ""
          else postR now num
  where
    postR time num = do
      state <- fetchIRCState
      let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth chan] & W.header "Client-ID" .~ [C.pack $ client_id $ botConfig state]
      cid <- liftIO $ query (db state) $ ChannelID $ channel chan
      rsp' <- liftIO $ safePostWith opts ("https://api.twitch.tv/kraken/channels/" ++ cid ++ "/commercial") $ object [("length", Number $ scientific (num * 30) 0)]
      case rsp' of
        Left err -> liftIO (logMsg (db state) chan Error err) >> pure ""
        Right rsp -> do
          let body = decode $ rsp ^. W.responseBody :: Maybe Value
          case body of
            Nothing -> pure ""
            Just (Object o) -> do
              liftIO $ update (db state) $ AddCommercial (channel chan) time (fromIntegral num)
              submode <- liftIO $ query (db state) $ FetchSetting (channel chan) "commercials:submode" Nothing
              case submode of
                Nothing -> pure ()
                Just _ -> do
                  let secs = round $ toRational $ num * 30 :: Int64
                  irc <- getIRCState
                  sendBS $ Privmsg (C.pack $ "#" ++ channel chan) (Right $ C.pack "/subscribers")
                  liftIO $ oneShotTimer (runIRCAction (sendBS $ Privmsg (C.pack $ "#" ++ channel chan) (Right $ C.pack "/subscribersoff")) irc) (sDelay secs)
                  pure ()
              pure $ (show num) ++ " commercials have been run."

commandPermit :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandPermit chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    let pnick = map toLower (if head (args!!0) == '@' then drop 1 (args!!0) else (args!!0))
    liftIO $ update (db state) $ Permit (channel chan) pnick
    liftIO $ oneShotTimer (update (db state) $ Unpermit (channel chan) pnick) (sDelay 30)
    pure $ pnick ++ " is permitted to post links for the next 30 seconds."

commandModeration :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandModeration chan args nick = do
  if length args < 2 then pure ""
  else do
    state <- fetchIRCState
    case head args of
      "blacklist" ->
        case args!!1 of
          "add" ->
            if length args < 4 then pure ""
            else do
              let regexS = L.intercalate " " $ drop 3 args
              regex' <- liftIO $ safeRegex [] regexS
              case regex' of
                Left e -> pure $ show e
                Right regex -> do
                  case (args!!2) of
                    "true" -> do
                      liftIO $ update (db state) $ AddBlacklist (channel chan) regexS True
                      pure $ regexS ++ " has been added to the blacklist."
                    "false" -> do
                      liftIO $ update (db state) $ AddBlacklist (channel chan) regexS False
                      pure $ regexS ++ " has been added to the blacklist."
                    _ -> pure "First argument needs to be either \"true\" or \"false\"."
          "remove" ->
            if length args < 3 then pure ""
            else do
              let regexS = L.intercalate " " $ drop 2 args
              liftIO $ update (db state) $ DelBlacklist (channel chan) regexS
              pure $ regexS ++ " has been removed from the blacklist."
          _ -> pure ""
      "links" ->
        case args!!1 of
          "add" ->
            if length args < 3 then pure ""
            else do
              liftIO $ update (db state) $ AddLink (channel chan) (args!!2)
              pure $ (args!!2) ++ " has been whitelisted."
          "remove" ->
            if length args < 3 then pure ""
            else do
              liftIO $ update (db state) $ DelLink (channel chan) (args!!2)
              pure $ (args!!2) ++ " has been removed from the whitelist."
          "list" -> do
            links <- liftIO $ query (db state) $ FetchLinks (channel chan)
            pure $ "/w " ++ nick ++ " " ++ L.intercalate " | " links
          _ -> pure ""
      "length" ->
        case args!!1 of
          "set" ->
            case (TR.readMaybe (args!!2) :: Maybe Int) of
              Nothing -> pure ""
              Just num -> do
                liftIO $ update (db state) $ SetLength (channel chan) num
                pure $ "Message length filter is set to: " ++ (args!!2)
          "remove" -> do
            liftIO $ update (db state) $ RemoveLength (channel chan)
            pure $ "Message length filter has been removed."
          _ -> pure ""
      "caps" ->
        case args!!1 of
          "set" ->
            if length args < 4 then pure ""
            else do
              case ((TR.readMaybe (args!!2) :: Maybe Int), (TR.readMaybe (args!!3) :: Maybe Int)) of
                (Just num2, Just num3) -> do
                  liftIO $ update (db state) $ SetCapFilter (channel chan) num2 num3
                  pure $ "Cap filter is set to " ++ (args!!2) ++ "% with a trigger limit of " ++ (args!!3) ++ "."
                _ -> pure ""
          "remove" -> do
            liftIO $ update (db state) $ RemoveCapFilter (channel chan)
            pure $ "Cap filter has been removed."
          _ -> pure ""
      "symbols" ->
        case args!!1 of
          "set" ->
            if length args < 4 then pure ""
            else do
              case ((TR.readMaybe (args!!2) :: Maybe Int), (TR.readMaybe (args!!3) :: Maybe Int)) of
                (Just num2, Just num3) -> do
                  liftIO $ update (db state) $ SetSymFilter (channel chan) num2 num3
                  pure $ "Symbol filter is set to " ++ (args!!2) ++ "% with a trigger limit of " ++ (args!!3) ++ "."
                _ -> pure ""
          "remove" -> do
            liftIO $ update (db state) $ RemoveSymFilter (channel chan)
            pure $ "Symbol filter has been removed."
          _ -> pure ""
      "colors" ->
        case args!!1 of
          "on" -> do
            liftIO $ update (db state) $ SetColorFilter (channel chan) True
            pure $ "Color filter has been turned on."
          "off" -> do
            liftIO $ update (db state) $ SetColorFilter (channel chan) False
            pure $ "Color filter has been turned off."
          _ -> pure ""
      "display" ->
        case args!!1 of
          "on" -> do
            liftIO $ update (db state) $ SetDisplayModeration (channel chan) True
            pure $ "Timeout reasons will be posted to chat."
          "off" -> do
            liftIO $ update (db state) $ SetDisplayModeration (channel chan) False
            pure $ "Timeout reasons will not be posted to chat."
          _ -> pure ""
      _ -> pure ""

commandWebAuth :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandWebAuth chan args nick = do
  state <- fetchIRCState
  auth <- liftIO nextRandom
  liftIO $ update (db state) $ SetWebAuth (channel chan) (UUID.toString auth)
  pure $ "web auth token has been regenerated"

commandUnmod :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandUnmod chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    liftIO $ update (db state) $ Unmod (channel chan) (head args)
    pure $ (head args) ++ " has been unmodded."

commandMod :: ChannelConfig -> [String] -> String -> IRC IrcState String
commandMod chan args nick = do
  if length args < 1 then pure ""
  else do
    state <- fetchIRCState
    liftIO $ update (db state) $ AddMod (channel chan) (head args)
    pure $ (head args) ++ " has been modded."
